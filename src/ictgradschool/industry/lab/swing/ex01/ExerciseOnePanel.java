package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton healthyButton;
    private JTextField heightTextField;
    private JTextField weightTextField;
    private JTextField bmiTextField;
    private JTextField healthyTextField;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        this.calculateBMIButton = new JButton("Calculate BMI");

        this.healthyButton = new JButton("Calculate Healthy Weight");

        this.heightTextField = new JTextField(20);

        this.weightTextField = new JTextField(20);

        this.bmiTextField = new JTextField(20);

        this.healthyTextField = new JTextField(20);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel  = new JLabel("Height in meters:");

        JLabel weightLabel  = new JLabel("Weight in kilograms:");

        JLabel bmiLabel  = new JLabel("Your Body Mass Index (BMI) is:");

        JLabel maximumLabel  = new JLabel("Maximum Healthy Weight for your Height:");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(heightTextField);

        this.add(weightLabel);
        this.add(weightTextField);

        this.add(calculateBMIButton);
        calculateBMIButton.addActionListener(this);

        this.add(bmiLabel);
        this.add(bmiTextField);

        this.add(healthyButton);
        healthyButton.addActionListener(this);

        this.add(maximumLabel);
        this.add(healthyTextField);

        // TODO Add Action Listeners for the JButtons
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        double weight = Double.parseDouble(weightTextField.getText());
        double height = Double.parseDouble(heightTextField.getText());

        if (event.getSource() == calculateBMIButton) {
          double BMI = weight / (height * height);
          bmiTextField.setText(" " + roundTo2DecimalPlaces(BMI));
        }else if(event.getSource() == healthyButton){
           double MaximumHealthyWeight = 24.9 * height * height;
           healthyTextField.setText(" " + roundTo2DecimalPlaces(MaximumHealthyWeight));
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}