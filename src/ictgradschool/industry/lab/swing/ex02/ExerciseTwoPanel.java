package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JTextField firstTextBox;
    private JTextField secondTextBox;
    private JButton addButton;
    private JButton subtractButton;
    private JTextField resultTextBox;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.firstTextBox = new JTextField(20);
        this.secondTextBox = new JTextField(20);
        this.addButton = new JButton("Add");
        this.subtractButton = new JButton("Subtract");
        this.resultTextBox = new JTextField(40);

        JLabel resultLabel = new JLabel("Result:");
        this.add(firstTextBox);

        this.add(secondTextBox);

        this.add(addButton);
        addButton.addActionListener(this);

        this.add(subtractButton);
        subtractButton.addActionListener(this);

        this.add(resultLabel);
        this.add(resultTextBox);
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        double int1 = Double.parseDouble(firstTextBox.getText());
        double int2 = Double.parseDouble(secondTextBox.getText());

        if(e.getSource() == addButton){
            double result = int1 + int2;
            resultTextBox.setText(" " + roundTo2DecimalPlaces(result));
        }else if (e.getSource() == subtractButton){
            double result = int1 - int2;
            resultTextBox.setText(" " + roundTo2DecimalPlaces(result));
        }
    }
}