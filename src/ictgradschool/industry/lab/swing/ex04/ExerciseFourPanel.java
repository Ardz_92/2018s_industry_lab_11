package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon balloon;
    private java.util.List<Balloon> balloonArray;
    private JButton moveButton;
    private Timer timer;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);
        this.balloonArray = new ArrayList<Balloon>();
        balloonArray.add(balloon);
        balloonArray.add(new Balloon(90, 50));
       /* this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);*/
        this.addKeyListener(this);
        this.timer = new Timer(100, this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (Balloon b : balloonArray) {
            b.move();
        }


        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon b : balloonArray) {
            b.draw(g);
        }
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {


        switch (e.getKeyCode()) {

            case KeyEvent.VK_RIGHT:
                for (Balloon b : balloonArray) {
                    b.setDirection(Direction.Right);
                }
                timer.start();
                break;

            case KeyEvent.VK_LEFT:
                for (Balloon b : balloonArray) {
                    b.setDirection(Direction.Left);
                }
                timer.start();
                break;

            case KeyEvent.VK_UP:
                for (Balloon b : balloonArray) {
                    b.setDirection(Direction.Up);
                }
                timer.start();
                break;

            case KeyEvent.VK_DOWN:
                for (Balloon b : balloonArray) {
                    b.setDirection(Direction.Down);
                }
                timer.start();
                break;

            case KeyEvent.VK_A:
                balloonArray.add(balloon);
                balloonArray.add(new Balloon((int) Math.random() * 40, (int) Math.random() * 40));
                break;

            case KeyEvent.VK_S:
                timer.stop();
                break;
        }
    }
    //balloon.move();
    // repaint();

    @Override
    public void keyReleased(KeyEvent e) {

    }
}